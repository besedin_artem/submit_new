$('select').dp_select();

(function($){
	jQuery.fn.dp_switcher = function(options){
		$.each(this, function(){
			var $el = $(this);
			$('.js_switcher').each(function(i, item){
				$(item).find('.js_switcher_item:first').addClass('active');
				$(item).siblings('.js_switcher_section:first').addClass('active');
			});
			$('select').dp_select();

			attachHandlers($el);

		});

		function attachHandlers($el) {
			$('.js_switcher_item').on('click', function(){
				var switcher = $(this).closest('.js_switcher');
				$(switcher).find('.active').removeClass('active');
				$(this).addClass('active');
				var index = $(this).index();

				$(switcher).siblings('.js_switcher_section.active').removeClass('active');
				$(switcher).siblings('.js_switcher_section:eq('+index+')').addClass('active');
				$(switcher).siblings('.js_switcher_section:eq('+index+')').find('select').dp_select();
			});
		}
	}
})(jQuery);

$('.js_switcher').dp_switcher();

    var $body = $('body')

    if ($body.hasClass('.personal')) {
        $("#phone").intlTelInput({
            onlyCountries: ["al", "ad", "at", "by", "be", "ba", "bg", "hr", "cz", "dk",
                "ee", "fo", "fi", "fr", "de", "gi", "gr", "va", "hu", "is", "ie", "it", "lv",
                "li", "lt", "lu", "mk", "mt", "md", "mc", "me", "nl", "no", "pl", "pt", "ro",
                "ru", "sm", "rs", "sk", "si", "es", "se", "ch", "ua", "gb"
            ],
            preferredCountries: ["ru"],
            utilsScript: "js/utils.js"
        });

    }
    if ($('#test_task__timer').size() != 0) {
        $('#test_task__timer').timeTo({
            timeTo: new Date(new Date('Fri May 20 2016 09:00:00 GMT+0500 (RTZ 4 (зима))')),
            displayDays: 2,
            fontSize: 36,
            displayCaptions: true,
            lang: 'ru'
        });
    }

    $('.js_switcher_item').on('click', function() {
        var switcher = $(this).closest('.js_switcher');
        $(this).addClass('active').siblings().removeClass('active');
        var index = $(this).index();

        $(switcher).siblings('.js_switcher_section.active').removeClass('active');
        $(switcher).siblings('.js_switcher_section:eq(' + index + ')').addClass('active');
        $(switcher).siblings('.js_switcher_section:eq(' + index + ')').find('select').dp_select();
    });


    if ($body.hasClass('portfolio')) {
        $body
			.on('click', function() {
				$('.js_portfolio_tool.show_popup').removeClass('show_popup');
			})
            .on('change', '.js_select_all_images', function() {
                $('.js_select_image').prop('checked', $(this).prop('checked')).trigger('change');
            })
            .on('change', '.js_select_image', function() {
                var $container = $(this).closest('.portfolio__image');
				($(this).prop('checked')?$container.addClass('checked'):$container.removeClass('checked'));
            })
            .on('blur', '.js_search_string', function() {
                var sstring = clear_sstring($(this).val());
				if (sstring.length == 0) return;
				var words = sstring.split(' ');
				var output = '';
				words.forEach(function(word) {
					output += '<li class="portfolio_search__word">' + word + '<span class="search_word__close js_word_remove"></span></li>';
				});
				$('.js_search_words').html(output);
            })
            .on('click', '.js_word_remove', function() {
				var words = $('.js_search_string').val().split(' ');
				delete words[$(this).parent().index()];
				$('.js_search_string').val(clear_sstring(words.join(' ')));	
				$(this).parent().remove();			
            })
			.on('click', '.js_portfolio_tool', function(e) {
				e.stopPropagation();
				$(this).toggleClass('show_popup').siblings().removeClass('show_popup');
			})
			.on('click', '.js_open_submenu', function(e){
				$(this).toggleClass('opened').find('.submenu').slideToggle();
				return false;
			})
			.on('click', '.js_open_submenu > ul', function(e){
				e.stopPropagation();
			});
		
		$('.js_search_string').blur();
			
		function clear_sstring(s) {
			return trimstrip(s.replace(/[^A-Za-zА-Яа-яЁё0-9\ ]/,'').replace(/\s\s+/g, ' '));
		}

		function trimstrip (s) {
			if (s != undefined) return s.replace(/^\s+|\s+$/g,"").replace(/\<\/?[^>]*>/ig,"");
			else return "";
		}	
		
    }

$(document).ready(function(){

	 jQuery('.scroll-pane').jScrollPane();


	if ($('body').hasClass('.personal')) {
			$("#phone").intlTelInput({onlyCountries: ["al", "ad", "at", "by", "be", "ba", "bg", "hr", "cz", "dk", 
			"ee", "fo", "fi", "fr", "de", "gi", "gr", "va", "hu", "is", "ie", "it", "lv", 
			"li", "lt", "lu", "mk", "mt", "md", "mc", "me", "nl", "no", "pl", "pt", "ro", 
			"ru", "sm", "rs", "sk", "si", "es", "se", "ch", "ua", "gb"], preferredCountries: ["ru"], utilsScript: "js/utils.js"});

	}
	if ($('#test_task__timer').size() != 0) {
		$('#test_task__timer').timeTo({
			timeTo: new Date(new Date('Fri May 20 2016 09:00:00 GMT+0500 (RTZ 4 (зима))')),
			displayDays: 2,
			fontSize: 36,
			displayCaptions: true,
			lang: 'ru'
		});
	}


	/*************************************/
	$('body').on('click', function(){
		$('.modal').fadeOut();
	});

	$('body').on('click', '.delete_account', function(e){
		e.preventDefault();
		$('#delete_question').fadeIn();
		return false;
	});

	$('body').on('click', '.delete_yes', function(e){
		e.preventDefault();
		$('.modal').fadeOut();
		$('#delete_yes').fadeIn();
		return false;
	});

	$('body').on('click', '.delete_no', function(e){
		e.preventDefault();
		$('.modal').fadeOut();
		$('#delete_no').fadeIn();
		return false;
	});

	$('.cabinet_open').on('click', function(){

	});

	$('body').on('click', '.personal_data__edit', function(e){
		e.preventDefault();
		$(this).closest('.personal_data__label ').addClass('editting');
		$('select').dp_select();
		
		/*var editting_text = $.trim($(this).closest('.personal_data__saved_data').text());
		$(this).closest('.personal_data__label ').find('input').val(editting_text);*/
		return false;
	});

	function MovingCabinet() {
		var menu_background = $('.js_menu_background');
		var cabinet_container = $('.js_moving_cabinet');
		var cabinet_open = $('.js_moving_cabinet_open');
		var cabinet_close = $('.js_moving_cabinet .close');

		$(cabinet_open).on('click', function(){
			$(menu_background).addClass('cabinet_active');
			$(cabinet_container).addClass('opened');
			$('.menu_widget_wrap').css('z-index', '10');
			$('body').css('overflow', 'hidden');
		});

		$(cabinet_close).on('click', function(){
			$(cabinet_container).removeClass('opened');
			$(menu_background).removeClass('cabinet_active');
		});

		$(cabinet_container).on('click', function(e){
			e.stopPropagation();
		});

		$(menu_background).on('click', function(){
			$(cabinet_container).removeClass('opened');
			$(menu_background).removeClass('cabinet_active');
			$('body').css('overflow', 'visible');

		});	

		$(document).keyup(function(e) { 
	        if (e.keyCode == 27) { // esc keycode
	            if ($(cabinet_container).hasClass('opened')) {
	            	$(cabinet_close).trigger('click');
	            }
	        }
	    });
	}

	var movingCabinet = new MovingCabinet();

});

