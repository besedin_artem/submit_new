/*
 * jquery.dp_select - v0.2.1 - 2016-05-25
 * <select> customization plugin with custom scroll
 * 
 * Copyright (c) 2016 DigiPro
 * 
 * Dependencies:
 * jquery, jScrollPane
 *
 * Markup made by plugin:
 * span.dp_select[style=width]>span.dp_select__current[data-value]+ul.dp_select__list>li.dp_select__item[data-value]
 *
 * Usage:
 * $('select').dp_select();
 */

(function($){
	jQuery.fn.dp_select = function(options) {
		$.each(this, function() {
			var $el = $(this),
				width = $el.width() + 12,
				html = '<span class="dp_select" style="width:' + width + 'px">',
				$options = $el.find('option'),
				$selected_option = $options.filter('[selected]');

			if ($selected_option.length === 0) $selected_option = $options.filter(':first-child');
			html += '<span class="dp_select__current" data-value="' + $selected_option.val() + '">' + $selected_option.text() + '</span>';
			html += '<ul class="dp_select__list">';
			$.each($options, function() {
				var $this = $(this);
				html += '<li class="dp_select__item" data-value="' + $this.val() + '">' + $this.text() + '</li>';
			});
			html += '</ul></span>';
			if ($el.next('.dp_select')[0]) {
				$el.next('.dp_select').remove();
			}
			$el.after(html).hide();
			attachHandlers($el);
		});

		return($(this).next('.dp_select'));

		function attachHandlers($el) {
			$el.next('.dp_select')
				.on('click', '.dp_select__current', function() {
					$('.dp_select_opened').removeClass('dp_select_opened').find('.dp_select__list').slideUp(100);
					if (!$(this).siblings('.dp_select__list').is(':visible')) {
						$(document).off('click.close_select');
						$(this).closest('.dp_select').addClass('dp_select_opened');
						var $list = $(this).closest('.dp_select').find('.dp_select__list');
						if ($list.find('li').length > 9) {
							$list.css('height', '145px');
							$list.slideDown(100, function() {initJScroll($(this));});
						}
						else {
							$list.slideDown(100);
						}
						$(document).on('click.close_select', function(e) {
							if (!$(e.target).closest('.dp_select')[0]) {
								$('.dp_select').removeClass('dp_select_opened').find('.dp_select__list').slideUp(100);
							}
						});
					}
				})
				.on('click', '.dp_select__item', function() {
					var $this = $(this),
						$fake_select = $this.closest('.dp_select'),
						$orig_select = $fake_select.prev('select');
					$fake_select
						.find('.dp_select__current').text($this.text()).attr('data-value', $this.data('value')).end()
						.removeClass('dp_select_opened').find('.dp_select__list').slideUp(100);
					$orig_select
						.find('option[selected]').prop('selected', false).end()
						.find('option[value=' + $this.data('value') + ']').prop('selected', true).end()
						.trigger('change');
				});
		}

		function initJScroll($el) {
			$el.on('mousewheel', function(e) {
				e.preventDefault();
				e.stopPropagation();
			});
			$el.jScrollPane({
				contentWidth: '0px'
			});
		}
	};
})(jQuery);